package com.fa.ims.constant;

public final class CommonConstants {

    public static final String USERNAME_SESSION = "USERNAME_SESSION";
    public static final String DEPARTMENT_OF_USER = "DEPARTMENT";
    public static final Integer PAGE_SIZE = 10;
}
