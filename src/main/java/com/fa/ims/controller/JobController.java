package com.fa.ims.controller;

import com.fa.ims.constant.CommonConstants;
import com.fa.ims.entity.Job;
import com.fa.ims.entity.JobStatus;
import com.fa.ims.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/job")
public class JobController {
    @Autowired
    private JobService jobService;
    @Autowired
    private SkillService skillService;
    @Autowired
    private BenefitService benefitService;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private JobStatusService jobStatusService;

    @GetMapping("/create-job")
    public String showCreateJob(Model model) {
        model.addAttribute("newJob", new Job());
        model.addAttribute("skills", skillService.findAll());
        model.addAttribute("benefits", benefitService.findAll());
        model.addAttribute("statusList", jobStatusService.findAll());
        return "job/create-job";
    }

    @PostMapping("/create-job")
    public String doCreateJob(@ModelAttribute("newJob") @Valid Job job,
                              BindingResult bindingResult,
                              Model model,
                              RedirectAttributes redirectAttributes,
                              @SessionAttribute(CommonConstants.USERNAME_SESSION) String usernameInSession) {
        model.addAttribute("skills", skillService.findAll());
        model.addAttribute("benefits", benefitService.findAll());
        model.addAttribute("statusList", jobStatusService.findAll());
        if (bindingResult.hasErrors()) {
            return "job/create-job";
        } else if (job.getStartDate().isBefore(LocalDate.now())) {
            bindingResult.reject("error.startDate", "The start date must be in the present or future.");
            return "job/create-job";
        }
        else if (job.getEndDate().isBefore(job.getStartDate())) {
            bindingResult.reject("error.endDate", "The end date must be after the start date");
            return "job/create-job";
        } else {
            job.setUser(authenticationService.findByUsername(usernameInSession)
                    .orElseThrow(() -> new UsernameNotFoundException("The username doesn't exist: " + usernameInSession)));
            job.setJobStatus(jobStatusService.findById(1L).orElse(new JobStatus()));
            jobService.createNew(job);
            return "redirect:/job/create-job";
        }
    }

    @GetMapping("/edit-job/{id}")
    public String showEditJob(Model model, @PathVariable("id") Long id) {
        Job job = jobService.findById(id).orElse(new Job());
        model.addAttribute("job", job);
        model.addAttribute("skills", skillService.findAll());
        model.addAttribute("benefits", benefitService.findAll());
        model.addAttribute("statusList", jobStatusService.findAll());
        return "job/edit-job";
    }

    @PostMapping("/edit-job")
    public String doEditJob(@ModelAttribute("job") @Valid Job job,
                            BindingResult bindingResult,
                            RedirectAttributes redirectAttributes,
                            Model model) {
        model.addAttribute("skills", skillService.findAll());
        model.addAttribute("benefits", benefitService.findAll());
        model.addAttribute("statusList", jobStatusService.findAll());
        if (bindingResult.hasErrors()) {
            return "job/edit-job";
        } else if (job.getEndDate().isBefore(job.getStartDate())) {
            bindingResult.reject("error.endDate", "The end date must be after the start date");
            return "job/edit-job";
        } else {
            jobService.update(job);
            return "redirect:/job/edit-job/" + job.getId();
        }
    }

    @GetMapping("/delete-job/{id}")
    public String doDeleteJob(@PathVariable("id") Long jobId,
                              RedirectAttributes redirectAttributes) {
        jobService.delete(jobId);
        return "redirect:/job/create-job";
    }

}
