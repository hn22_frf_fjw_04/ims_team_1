package com.fa.ims.controller;

import com.fa.ims.constant.CommonConstants;
import com.fa.ims.entity.Schedule;
import com.fa.ims.enums.InterviewSearchOption;
import com.fa.ims.enums.InterviewStatus;
import com.fa.ims.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/schedule")
public class ScheduleController {
    @Autowired
    private ScheduleService scheduleService;

    @GetMapping("/view")
    public String showInterviewSchedule(@RequestParam(defaultValue = "", required = false) String keyword,
                                        @RequestParam( required = false) InterviewSearchOption option,
                                        @RequestParam( required = false) InterviewStatus status,
                                        Model model,
                                        @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex) {

        List<Schedule> scheduleList = scheduleService.findSchedulesWithSearchOptionAndStatus(option,status,keyword,PageRequest.of(pageIndex-1, CommonConstants.PAGE_SIZE)).toList();
        List<Schedule> scheduleFullList = scheduleService.findSchedulesWithSearchOptionAndStatus(option,status,keyword,PageRequest.of(0,Integer.MAX_VALUE)).toList();

        int totalPages = scheduleFullList.size()/CommonConstants.PAGE_SIZE < 1 ? 1 : (scheduleFullList.size()/(CommonConstants.PAGE_SIZE+1) + 1);
        int currentPage = 0;
        if (scheduleList.size() == CommonConstants.PAGE_SIZE) {
            currentPage = CommonConstants.PAGE_SIZE*pageIndex;
        } else {currentPage = CommonConstants.PAGE_SIZE*(pageIndex-1) + scheduleList.size();}

        model.addAttribute("statusList", Arrays.stream(InterviewStatus.values()).collect(Collectors.toList()));
        model.addAttribute("searchOptions", Arrays.stream(InterviewSearchOption.values()).collect(Collectors.toList()));
        model.addAttribute("scheduleList", scheduleList);
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("option", option);
        model.addAttribute("status", status);
        model.addAttribute("keyword", keyword);
        model.addAttribute("totalRows", scheduleFullList.size());
        model.addAttribute("currentPage", currentPage);
        return "interview-schedule/view";
    }

}
