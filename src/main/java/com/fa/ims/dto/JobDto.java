package com.fa.ims.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
public class JobDto {
    @NotBlank(message = "Job title cannot be left blank")
    private String jobTitle;
    @NotBlank(message = "Start date cannot be left blank")
    private String startDate;
    @NotBlank(message = "End date cannot be left blank")
    private String endDate;
    private String workingAddress;
    private String description;
    @NotBlank(message = "Minimum salary cannot be left blank")
    private String salaryRangeFrom;
    @NotBlank(message = "Maximum salary cannot be left blank")
    private String salaryRangeTo;

    private List<String> skillsId;

    private List<String> benefitsId;
}
