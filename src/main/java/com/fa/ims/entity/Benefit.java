package com.fa.ims.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "benefit")
public class Benefit extends BaseEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @Column(name = "benefit")
    private String benefit;

    @Column(name = "description")
    private String description;


    @ManyToMany(mappedBy = "benefits")
    private List<Job> jobs = new ArrayList<>();

}