package com.fa.ims.entity;

import com.fa.ims.enums.CandidateStatus;
import com.fa.ims.enums.Gender;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "candidate")
public class Candidate extends BaseEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "candidate_status")
    private CandidateStatus candidateStatus;

    @Column(name = "year_of_experience")
    private Integer yearOfExperience;

    @ManyToOne
    @JoinColumn(name = "highest_level_id")
    private HighestLevel highestLevel;

    @ManyToOne
    @JoinColumn(name = "recruiter_id_id")
    private User recruiterId;

    @Column(name = "note")
    private String note;

    @ManyToMany
    @JoinTable(name = "candidate_skills",
            joinColumns = @JoinColumn(name = "candidate_id"),
            inverseJoinColumns = @JoinColumn(name = "skills_id"))
    private List<Skill> skills = new ArrayList<>();

    @OneToMany(mappedBy = "candidate")
    private List<Schedule> schedules = new ArrayList<>();

}