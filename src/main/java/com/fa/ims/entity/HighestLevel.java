package com.fa.ims.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "highest_level")
public class HighestLevel extends BaseEntity{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "highest_level")
    private String highestLevel;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "highestLevel")
    private List<Candidate> candidates = new ArrayList<>();

}