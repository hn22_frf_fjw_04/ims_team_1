package com.fa.ims.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "job")
public class Job extends BaseEntity{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "job_title")
    @NotBlank(message = "Job title cannot be left blank")
    private String jobTitle;

    @Column(name = "start_date", columnDefinition = "date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Cannot left start date blank")
    private LocalDate startDate;

    @Column(name = "end_date", columnDefinition = "date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Cannot left end date blank")
    private LocalDate endDate;

    @Column(name = "salary_range_from")
    @NotNull(message = "Minimum salary cannot be left blank")
    private Long salaryRangeFrom;

    @Column(name = "salary_range_to")
    @NotNull(message = "Maximum salary cannot be left blank")
    private Long salaryRangeTo;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "working_address")
    private String workingAddress;

    @Column(name = "description")
    private String description;
    @ToString.Exclude
    @ManyToMany
    @JoinTable(name = "job_skills",
            joinColumns = @JoinColumn(name = "job_id"),
            inverseJoinColumns = @JoinColumn(name = "skills_id"))
    @Size(min = 1, max = 6, message = "At least 1 and maximum of 6 skills must be selected.")
    private List<Skill> skills = new ArrayList<>();
    @ToString.Exclude
    @ManyToMany
    @JoinTable(name = "job_benefits",
            joinColumns = @JoinColumn(name = "job_id"),
            inverseJoinColumns = @JoinColumn(name = "benefits_id"))
    @Size(min = 1, max = 6, message = "At least 1 and maximum of 6 benefits must be selected.")
    private List<Benefit> benefits = new ArrayList<>();

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "status_id")
    private JobStatus jobStatus;

}