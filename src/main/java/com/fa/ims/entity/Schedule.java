package com.fa.ims.entity;

import com.fa.ims.enums.InterviewResult;
import com.fa.ims.enums.InterviewStatus;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "schedule")
public class Schedule extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "schedule_title")
    private String scheduleTitle;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;

    @Column(name = "schedule_date")
    private LocalDate scheduleDate;

    @Column(name = "schedule_time_start")
    private LocalTime scheduleTimeStart;

    @Column(name = "schedule_time_end")
    private LocalTime scheduleTimeEnd;

    @Column(name = "notes")
    private String notes;


    @Column(name = "location")
    private String location;


    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "recruiter_owner_id")
    private User recruiterOwner;

    @Column(name = "meeting_id")
    private String meetingId;

    @Enumerated(EnumType.STRING)
    @Column(name = "interview_result")
    private InterviewResult interviewResult;

    @ToString.Exclude
    @ManyToMany
    @JoinTable(name = "schedule_users",
            joinColumns = @JoinColumn(name = "schedule_id"),
            inverseJoinColumns = @JoinColumn(name = "users_id"))
    private List<User> interviewers = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "interview_status")
    private InterviewStatus interviewStatus;

}