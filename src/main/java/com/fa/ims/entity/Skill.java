package com.fa.ims.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "skill")
public class Skill extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "skill")
    private String skill;

    @ManyToMany(mappedBy = "skills")
    private List<Candidate> candidates = new ArrayList<>();

    @ManyToMany(mappedBy = "skills")
    private List<Job> jobs = new ArrayList<>();

}