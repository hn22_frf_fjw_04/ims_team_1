package com.fa.ims.enums;

public enum CandidateStatus {
    OPEN, BANNED;
}
