package com.fa.ims.enums;

public enum Gender {
    MALE, FEMALE;
}
