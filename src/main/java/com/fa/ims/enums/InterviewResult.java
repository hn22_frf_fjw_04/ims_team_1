package com.fa.ims.enums;

public enum InterviewResult {
    Open, Failed, Pass, Cancel;
}
