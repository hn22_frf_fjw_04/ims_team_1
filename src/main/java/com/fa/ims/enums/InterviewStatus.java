package com.fa.ims.enums;

public enum InterviewStatus {
    Open, Fail, Pass, Cancel;
}
