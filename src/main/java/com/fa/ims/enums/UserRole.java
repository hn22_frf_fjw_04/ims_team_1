package com.fa.ims.enums;

public enum UserRole {
    ROLE_RECRUITER, ROLE_INTERVIEWER, ROLE_MANAGER, ROLE_ADMIN
}
