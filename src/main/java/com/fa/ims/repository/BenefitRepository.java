package com.fa.ims.repository;

import com.fa.ims.entity.Benefit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BenefitRepository extends BaseRepository<Benefit, Long> {
}
