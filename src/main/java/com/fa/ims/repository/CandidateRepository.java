package com.fa.ims.repository;

import com.fa.ims.entity.Candidate;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateRepository  extends  BaseRepository<Candidate, Long>{
}
