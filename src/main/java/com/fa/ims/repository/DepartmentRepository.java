package com.fa.ims.repository;

import com.fa.ims.entity.Department;

public interface DepartmentRepository extends BaseRepository<Department, Long>{
}
