package com.fa.ims.repository;

import com.fa.ims.entity.HighestLevel;

public interface HighestLevelRepository extends BaseRepository<HighestLevel,Long> {
}
