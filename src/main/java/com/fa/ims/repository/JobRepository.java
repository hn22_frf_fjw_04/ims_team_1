package com.fa.ims.repository;

import com.fa.ims.entity.Job;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends BaseRepository<Job, Long> {
}
