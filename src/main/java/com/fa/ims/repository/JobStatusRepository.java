package com.fa.ims.repository;

import com.fa.ims.entity.JobStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobStatusRepository extends BaseRepository<JobStatus, Long> {
}
