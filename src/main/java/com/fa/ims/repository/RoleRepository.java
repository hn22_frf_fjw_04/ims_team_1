package com.fa.ims.repository;

import com.fa.ims.entity.Role;

public interface RoleRepository extends BaseRepository<Role, Long> {
}
