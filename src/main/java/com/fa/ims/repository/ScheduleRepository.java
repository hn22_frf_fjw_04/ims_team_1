package com.fa.ims.repository;

import com.fa.ims.entity.Schedule;
import com.fa.ims.enums.InterviewStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;

public interface ScheduleRepository extends BaseRepository<Schedule, Long> {
    Page<Schedule> findByInterviewStatus(InterviewStatus interviewStatus, Pageable pageable);
    Page<Schedule> findByScheduleTitleContainsIgnoreCaseAndInterviewStatus(String scheduleTitle, InterviewStatus interviewStatus, Pageable pageable);

    Page<Schedule> findByCandidate_FullNameContainsIgnoreCaseAndInterviewStatus(String fullName, InterviewStatus interviewStatus, Pageable pageable);

    Page<Schedule> findByInterviewers_FullNameContainsIgnoreCaseAndInterviewStatus(String fullName, InterviewStatus interviewStatus, Pageable pageable);

    Page<Schedule> findByCandidate_FullNameContainsIgnoreCase(String keyword, Pageable pageable);

    Page<Schedule> findByInterviewers_FullNameContainsIgnoreCase(String keyword, Pageable pageable);

    Page<Schedule> findByScheduleTitleContainsIgnoreCase(String keyword, Pageable pageable);
}
