package com.fa.ims.repository;

import com.fa.ims.entity.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User, Long> {

    Optional<User> findByUsernameAndDeletedFalse(String username);
}
