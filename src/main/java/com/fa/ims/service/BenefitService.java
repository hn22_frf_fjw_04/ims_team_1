package com.fa.ims.service;

import com.fa.ims.entity.Benefit;

public interface BenefitService extends BaseService<Benefit, Long>{
}
