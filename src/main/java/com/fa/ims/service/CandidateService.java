package com.fa.ims.service;

import com.fa.ims.entity.Candidate;

public interface CandidateService extends BaseService<Candidate, Long> {
}
