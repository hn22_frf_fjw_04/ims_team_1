package com.fa.ims.service;

import com.fa.ims.entity.Department;

public interface DepartmentService extends BaseService<Department, Long>{
}
