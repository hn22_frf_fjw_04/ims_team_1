package com.fa.ims.service;

import com.fa.ims.entity.HighestLevel;

public interface HighestLevelService extends BaseService<HighestLevel, Long>{
}
