package com.fa.ims.service;

import com.fa.ims.dto.JobDto;
import com.fa.ims.entity.Job;

public interface JobService extends BaseService<Job, Long>{
    void createNewFromJobDto(JobDto jobDto);
}
