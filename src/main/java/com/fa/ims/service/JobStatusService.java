package com.fa.ims.service;

import com.fa.ims.entity.JobStatus;

public interface JobStatusService extends BaseService<JobStatus, Long> {
}
