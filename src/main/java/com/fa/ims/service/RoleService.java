package com.fa.ims.service;

import com.fa.ims.entity.Role;

public interface RoleService extends BaseService<Role, Long>{
}
