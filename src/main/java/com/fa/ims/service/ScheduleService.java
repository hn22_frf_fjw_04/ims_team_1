package com.fa.ims.service;

import com.fa.ims.entity.Schedule;
import com.fa.ims.enums.InterviewSearchOption;
import com.fa.ims.enums.InterviewStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public interface ScheduleService extends BaseService<Schedule, Long>{

    Page<Schedule> findSchedulesWithSearchOptionAndStatus(InterviewSearchOption searchOption, InterviewStatus status, String keyword, Pageable pageable);

}
