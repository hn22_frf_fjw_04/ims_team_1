package com.fa.ims.service;

import com.fa.ims.entity.Skill;

public interface SkillService extends BaseService<Skill, Long>{
}
