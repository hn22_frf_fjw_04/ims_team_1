package com.fa.ims.service;

import com.fa.ims.entity.User;

public interface UserService extends BaseService<User, Long>{
}
