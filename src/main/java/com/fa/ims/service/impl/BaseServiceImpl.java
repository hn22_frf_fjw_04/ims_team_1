package com.fa.ims.service.impl;

import com.fa.ims.entity.BaseEntity;
import com.fa.ims.repository.BaseRepository;
import com.fa.ims.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class BaseServiceImpl<E extends BaseEntity, ID extends Serializable, R extends BaseRepository<E, ID>>
    implements BaseService<E, ID> {

    protected R repository;

    @Autowired
    public void setRepository(R repository) {
        this.repository = repository;
    }

    @Override
    public E createNew(E entity) {
        entity.setDeleted(false);
        return repository.save(entity);
    }

    @Override
    public E update(E entity) {
        return repository.save(entity);
    }

    @Override
    public void physicalDelete(ID id) {
        repository.deleteById(id);
    }

    @Override
    public void delete(ID id) {
        E entity = repository.findById(id).orElseThrow(IllegalArgumentException::new);
        delete(entity);
    }

    @Override
    public void delete(E entity) {
        entity.setDeleted(true);
        repository.save(entity);
    }

    @Override
    public Optional<E> findById(ID id) {
        return repository.findByIdAndDeletedFalse(id);
    }

    @Override
    public Page<E> findAllWithPage(Pageable page) {
        return repository.findAll(page);
    }

    @Override
    public List<E> findAll() {

        return repository.findAllByDeletedIsFalse();
    }
}
