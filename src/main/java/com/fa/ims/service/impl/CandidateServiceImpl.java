package com.fa.ims.service.impl;

import com.fa.ims.entity.Candidate;
import com.fa.ims.repository.CandidateRepository;
import com.fa.ims.service.CandidateService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CandidateServiceImpl extends BaseServiceImpl<Candidate, Long, CandidateRepository> implements CandidateService {
}
