package com.fa.ims.service.impl;

import com.fa.ims.entity.Department;
import com.fa.ims.repository.DepartmentRepository;
import com.fa.ims.service.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DepartmentServiceImpl extends BaseServiceImpl<Department, Long, DepartmentRepository> implements DepartmentService {
}
