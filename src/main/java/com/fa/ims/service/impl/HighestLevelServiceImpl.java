package com.fa.ims.service.impl;

import com.fa.ims.entity.HighestLevel;
import com.fa.ims.repository.HighestLevelRepository;
import com.fa.ims.service.HighestLevelService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HighestLevelServiceImpl extends BaseServiceImpl<HighestLevel, Long, HighestLevelRepository> implements HighestLevelService {
}
