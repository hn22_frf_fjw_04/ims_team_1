package com.fa.ims.service.impl;

import com.fa.ims.dto.JobDto;
import com.fa.ims.entity.Benefit;
import com.fa.ims.entity.Job;
import com.fa.ims.entity.Skill;
import com.fa.ims.entity.JobStatus;
import com.fa.ims.repository.BenefitRepository;
import com.fa.ims.repository.JobRepository;
import com.fa.ims.repository.SkillRepository;
import com.fa.ims.repository.JobStatusRepository;
import com.fa.ims.service.JobService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class JobServiceImpl extends BaseServiceImpl<Job, Long, JobRepository> implements JobService {
    @Autowired
    private SkillRepository skillRepository;

    @Autowired
    private BenefitRepository benefitRepository;

    @Autowired
    private JobStatusRepository jobStatusRepository;

    @Override
    public void createNewFromJobDto(JobDto jobDto) {

        List<Benefit> benefitsOfJob = new ArrayList<>();
        jobDto.getBenefitsId().forEach(id -> {
            benefitsOfJob.add(benefitRepository.findById(Long.parseLong(id)).get());
        });

        List<Skill> skillsOfJob = new ArrayList<>();
        jobDto.getSkillsId().forEach(id -> {
            skillsOfJob.add(skillRepository.findById(Long.parseLong(id)).get());
        });

        Job job = Job.builder()
                .jobTitle(jobDto.getJobTitle())
                .salaryRangeFrom(Long.parseLong(jobDto.getSalaryRangeFrom()))
                .salaryRangeTo(Long.parseLong(jobDto.getSalaryRangeTo()))
                .startDate(LocalDate.parse(jobDto.getStartDate()))
                .endDate(LocalDate.parse(jobDto.getEndDate()))
                .workingAddress(jobDto.getWorkingAddress())
                .description(jobDto.getDescription())
                .benefits(benefitsOfJob)
                .skills(skillsOfJob)
                .jobStatus(jobStatusRepository.findById(1L).orElse(new JobStatus()))
                .build();

        createNew(job);
    }
}
