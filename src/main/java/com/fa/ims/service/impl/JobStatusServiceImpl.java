package com.fa.ims.service.impl;

import com.fa.ims.entity.JobStatus;
import com.fa.ims.repository.JobStatusRepository;
import com.fa.ims.service.JobStatusService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class JobStatusServiceImpl extends BaseServiceImpl<JobStatus, Long, JobStatusRepository> implements JobStatusService {
}
