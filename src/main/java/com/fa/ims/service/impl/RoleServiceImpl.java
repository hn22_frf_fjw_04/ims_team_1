package com.fa.ims.service.impl;

import com.fa.ims.entity.Role;
import com.fa.ims.repository.RoleRepository;
import com.fa.ims.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoleServiceImpl extends BaseServiceImpl<Role, Long, RoleRepository> implements RoleService {
}
