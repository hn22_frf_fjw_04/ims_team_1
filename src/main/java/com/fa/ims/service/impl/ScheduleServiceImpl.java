package com.fa.ims.service.impl;

import com.fa.ims.entity.Schedule;
import com.fa.ims.enums.InterviewSearchOption;
import com.fa.ims.enums.InterviewStatus;
import com.fa.ims.repository.ScheduleRepository;
import com.fa.ims.service.ScheduleService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ScheduleServiceImpl extends BaseServiceImpl<Schedule, Long, ScheduleRepository> implements ScheduleService {
    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    public Page<Schedule> findSchedulesWithSearchOptionAndStatus(InterviewSearchOption searchOption, InterviewStatus status, String keyword, Pageable pageable) {
        Page<Schedule> schedules;
        if (searchOption == null && status == null) {
            schedules = scheduleRepository.findByScheduleTitleContainsIgnoreCase(keyword,pageable);
        } else if (searchOption == null) {
            schedules = scheduleRepository.findByInterviewStatus(status, pageable);
        } else if (status == null) {
            switch (searchOption) {
                case Candidate:
                    schedules = scheduleRepository.findByCandidate_FullNameContainsIgnoreCase(keyword, pageable);
                    break;
                case Interviewer:
                    schedules = scheduleRepository.findByInterviewers_FullNameContainsIgnoreCase(keyword, pageable);
                    break;
                default:
                    schedules = scheduleRepository.findByScheduleTitleContainsIgnoreCase(keyword, pageable);
                    break;
            }
        }
        else {
            switch (searchOption) {
                case Candidate:
                    schedules = scheduleRepository.findByCandidate_FullNameContainsIgnoreCaseAndInterviewStatus(keyword, status, pageable);
                    break;
                case Interviewer:
                    schedules = scheduleRepository.findByInterviewers_FullNameContainsIgnoreCaseAndInterviewStatus(keyword, status, pageable);
                    break;
                default:
                    schedules = scheduleRepository.findByScheduleTitleContainsIgnoreCaseAndInterviewStatus(keyword, status, pageable);
                    break;
            }
        }

        return schedules;
    }

}
