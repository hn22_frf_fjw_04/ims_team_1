package com.fa.ims.service.impl;

import com.fa.ims.entity.User;
import com.fa.ims.repository.UserRepository;
import com.fa.ims.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl extends BaseServiceImpl<User, Long, UserRepository> implements UserService {
}
