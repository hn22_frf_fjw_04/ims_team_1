CREATE SCHEMA `dev_ims_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin ;

-- admin/admin
INSERT INTO `users`
(`created_by`, `created_date`, `last_modified_by`, `last_modified_date`, `deleted`, `email`, `password`, `user_role`, `username`)
VALUES ('system', '2022-09-25 00:00:00', 'system', '2022-09-25 00:00:00', false, 'admin@g.com', '$2a$10$AB9H4H705JZfWgNxXKfVPe6l.A.7fxG6GI9eHhzT.SB5SCFMai6yq', 'ROLE_ADMIN', 'admin');

-- manager/manager
INSERT INTO `users`
(`created_by`, `created_date`, `last_modified_by`, `last_modified_date`, `deleted`, `email`, `password`, `user_role`, `username`)
VALUES ('system', '2022-09-25 00:00:00', 'system', '2022-09-25 00:00:00', false, 'manager@g.com', '$2a$10$22sXKkz5T3sERURmDTV6puuZA35j5Zo6gaw4gkrKLG1mBe/aRhX9W', 'ROLE_MANAGER', 'manager');

-- recruiter/recruiter
INSERT INTO `users`
(`created_by`, `created_date`, `last_modified_by`, `last_modified_date`, `deleted`, `email`, `password`, `user_role`, `username`)
VALUES ('system', '2022-09-25 00:00:00', 'system', '2022-09-25 00:00:00', false, 'recruiter@g.com', '$2a$10$3TBR08GmhsKCJK24qopE7ew.3eAaTdiXF1QTtodcx5mrjo0jFrbky', 'ROLE_RECRUITER', 'recruiter');

-- interviewer/interviewer
INSERT INTO `users`
(`created_by`, `created_date`, `last_modified_by`, `last_modified_date`, `deleted`, `email`, `password`, `user_role`, `username`)
VALUES ('system', '2022-09-25 00:00:00', 'system', '2022-09-25 00:00:00', false, 'interviewer@g.com', '$2a$10$V31ECnlzE.k7gTuwVChJZ.tOG5YzALkJLnr682cp7J4o08pJd7hjS', 'ROLE_INTERVIEWER', 'interviewer');